@echo off
cls

set MAIN_PATH="%~dp0.."
set IP_ADDR=%1
set PORT_NUM=%2
set PATH=%MAIN_PATH%\Java\bin;%MAIN_PATH%\Java\ant-1.9.6\bin;c:\Windows

ant client -Ddrive-letter=%MAIN_PATH% -Dip-address=%IP_ADDR% -Dport-num=%PORT_NUM%

